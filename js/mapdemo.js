var cars = {},
        map, circle;

// For creating the map markers
var moveRef = new Firebase("https://crackling-fire-1878.firebaseio.com/list/");
console.log(moveRef);
var geoRef = new Firebase("https://crackling-fire-1878.firebaseio.com/location/geoFire");
var hashRef = geoRef.child("/dataByHash");
var idRef = geoRef.child("/dataById");
console.log(geoRef);
// For the search
var geo = new geoFire(new Firebase("https://crackling-fire-1878.firebaseio.com/location/"));
var center = [13.789352635274554, 100.57449260518797];
var src = [13.789352635274554, 100.57449260518797];
var radiusInKm = 5.5;

// UI Elements
var $console;
var $consoleList;
function initializeMap() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            center = [position.coords.latitude, position.coords.longitude];
            src = [position.coords.latitude, position.coords.longitude];
            initializeMap2();
        });
    }
    initializeMap2();

}

function initializeMap2() {

    $console = $('#location-console');
    $consoleList = $('#location-console ul');
    console.log(center);
    loc = new google.maps.LatLng(center[0], center[1]);
    var mapOptions = {
        center: loc,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);

    circleLoc = new google.maps.LatLng(src[0], src[1]);
    var circleOptions = {
        strokeColor: "#6D3099",
        strokeOpacity: 0.7,
        strokeWeight: 1,
        fillColor: "#B650FF",
        fillOpacity: 0.0,
        map: map,
        center: circleLoc,
        radius: ((radiusInKm) * 1000),
        draggable: false
    };

    circle = new google.maps.Circle(circleOptions);

    google.maps.event.addListener(circle, 'dragend', function() {
        //console.log(circle.getCenter());
        map.setCenter(circle.getCenter());
        src = circle.getCenter();
        latt = map.getCenter().lat();
        longg = map.getCenter().lng();
        src = [latt, longg];
        search(src);

    });


//http://jsfiddle.net/fJ4jG/9/
    google.maps.event.addListener(map, "center_changed", function() {

        src = circle.setCenter(new google.maps.LatLng(map.getCenter().lat(), map.getCenter().lng()));
        latt = map.getCenter().lat();
        longg = map.getCenter().lng();
        src = [latt, longg];
        search(src);
    });


    moveRef.once("value", function(snapshot) {
        snapshot.forEach(function(car) {
            // console.log(car.val());
            createCar(car.val(), car.name());
        });
    });
    src = circle.getCenter();
    latt = map.getCenter().lat();
    longg = map.getCenter().lng();
    src = [latt, longg];

    search(src);
}



moveRef.on("child_changed", function(snapshot) {
    var marker = cars[snapshot.name()];
    if (typeof marker === 'undefined') {
        createCar(snapshot.val(), snapshot.name());
    }
    else {
        //var loc = geoFire.decode(snapshot.val().geohash);
        //marker.animatedMoveTo(loc[0], loc[1]);
        marker.animatedMoveTo(snapshot.val().lat, snapshot.val().lon);
    }
});

moveRef.on("child_removed", function(snapshot) {
    var marker = cars[snapshot.name()];
    if (typeof marker !== 'undefined') {
        marker.setMap(null);
        delete cars[snapshot.name()];
    }
});
function search(center)
{
    geo.onPointsNearLoc(center, radiusInKm, function(allBuses) {
        var a = allBuses;
        var a = _.uniq(a, function(item, key, a) {
            return item.a
        });
        if (allBuses.length > 0) {
            $consoleList.html('');
            $console.removeClass('hidden').addClass('bounceInUp');
            var buses = [];

            _.map(allBuses, function(bus) {

                buses.push(bus.name);

                var car2 = {id: bus.id};
                geo.getLocById(car2.id, function(latLon) {
                    if (latLon)
                    {
                        var dis = distance(center[0], center[1], latLon[0], latLon[1]);
                        //console.log(center, location);
                        //console.log(dis);
                    }
                });
            });
            var uniqueBuses = _.unique(buses);
            // Loop through and add coordinates
            _.each(uniqueBuses, function(bus) {
                $consoleList.append('<li>' + bus + '</li>');

            });
        } else
        {
            $consoleList.html('');

        }
    });
}
function createCar(car, firebaseId) {
    //console.log(car.data.latitude + " " + car.data.name);
    var latLon = new google.maps.LatLng(car.data.latitude, car.data.longitude);
    var dirColor = car.dirTag && car.dirTag.indexOf('OB') > -1 ? "50B1FF" : "FF6450";
    var iconType = 'cafe'; // 'train' looks nearly identical to bus at rendered size
    var marker = new google.maps.Marker({position: latLon,
        map: map});
    cars[firebaseId] = marker;
}


function feq(f1, f2) {
    return (Math.abs(f1 - f2) < 0.000001);
}

// Vikrum's hack to animate/move the Marker class
// based on http://stackoverflow.com/a/10906464
google.maps.Marker.prototype.animatedMoveTo = function(toLat, toLng) {
    var fromLat = this.getPosition().lat();
    var fromLng = this.getPosition().lng();

    if (feq(fromLat, toLat) && feq(fromLng, toLng))
        return;

    // store a LatLng for each step of the animation
    var frames = [];
    for (var percent = 0; percent < 1; percent += 0.005) {
        curLat = fromLat + percent * (toLat - fromLat);
        curLng = fromLng + percent * (toLng - fromLng);
        frames.push(new google.maps.LatLng(curLat, curLng));
    }

    move = function(marker, latlngs, index, wait) {
        marker.setPosition(latlngs[index]);
        if (index != latlngs.length - 1) {
            // call the next "frame" of the animation
            setTimeout(function() {
                move(marker, latlngs, index + 1, wait);
            }, wait);
        }
    };

    // begin animation, send back to origin after completion
    move(this, frames, 0, 25);
};


function distance(lat1, lon1, lat2, lon2) {
    var R = 6371; // km (change this constant to get miles)
    var dLat = (lat2 - lat1) * Math.PI / 180;
    var dLon = (lon2 - lon1) * Math.PI / 180;
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    /*if (d > 1)
     return Math.round(d) + "km";
     else if (d <= 1)
     return Math.round(d * 1000) + "m";*/
    return Math.round(d * 1000);
}        