
function updateMarkerPosition(latLng, $scope) {
    window.xxx = $scope;
    if (typeof $scope.project.data == 'undefined')
    {
        $scope.project.data = {};
    }
    document.getElementById('lat').value = latLng.lat();
    document.getElementById('lng').value = latLng.lng();
    $scope.project.data.latitude = latLng.lat();
    $scope.project.data.longitude = latLng.lng();
    console.log(window.xxx);
}

function initialize($scope) {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            updatePosition(latLng, $scope, 'mapCanvas');
        });
    }
}

function updatePosition(latLng, $scope, id, drag)
{
    var map = new google.maps.Map(document.getElementById(id), {
        zoom: 17,
        center: latLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marker = new google.maps.Marker({
        position: latLng,
        title: 'Your Here !!',
        map: map,
        draggable: true
    });

    // Update current position info.
    map.setCenter(latLng);
    updateMarkerPosition(latLng, $scope);

    google.maps.event.addListener(marker, 'dragend', function() {
        updateMarkerPosition(marker.getPosition(), $scope);
    });
}

function addMap(lat, lon, id, drag, $scope) {
    if (lat && lon) {
        var newPosition = new google.maps.LatLng(lat, lon);
        updatePosition(newPosition, $scope, id, drag);
    } else
    {
        initialize($scope);
    }
    console.log(lon);
}
