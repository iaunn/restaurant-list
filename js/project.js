angular.module('restaurant', ['ngRoute', 'firebase', 'ngMap', 'ui.utils'])
        .value('fbURL', 'https://crackling-fire-1878.firebaseio.com/list/')
        .value('fbgeo', 'https://crackling-fire-1878.firebaseio.com/location/')
        .factory('RestaurantService', function($firebase, fbURL) {
            return $firebase(new Firebase(fbURL));
        })
        .service('$menuService', function() {
            this.listactive = "";
            this.mapactive = "";
        })
        .config(function($routeProvider) {
            $routeProvider
                    .when('/', {
                        controller: 'ListList',
                        templateUrl: 'list.html'
                    })
                    .when('/edit/:projectId', {
                        controller: 'EditList',
                        templateUrl: 'detail.html'
                    })
                    .when('/new', {
                        controller: 'CreateList',
                        templateUrl: 'detail.html'
                    })
                    .when('/map', {
                        controller: 'MapController',
                        templateUrl: 'map.html'
                    })
                    .otherwise({
                        redirectTo: '/'
                    });
        });


function MenuCtrl($scope, $menuService) {
    $scope.listactive = $menuService.listactive;
    $scope.mapactive = $menuService.mapactive;
    $scope.$menuService = $menuService;
}

function ListList($scope, RestaurantService, fbURL, $firebase, $menuService) {
    $scope.projects = RestaurantService;
    $scope.checkopen = function(name, time) {
        var opennow = new Firebase(fbURL + name);

        $menuService.listactive = "active";
        $menuService.mapactive = "";

        var open = false;
        if (time === undefined)
        {
            open = 'nottime';
        }

        for (var index in time) {

            var attr = time[index];
            var date = attr.date;

            var timeopen = attr.open;
            timeopen1 = timeopen.substring(0, 2);
            timeopen2 = timeopen.substring(2, 4);
            var timeclose = attr.close;
            timeclose1 = timeclose.substring(0, 2);
            timeclose2 = timeclose.substring(2, 4);


            var start = new Date();
            start.setHours(parseInt(timeopen1), timeopen2);

            var stop = new Date();
            stop.setHours(parseInt(timeclose1), timeclose2);
            var now = new Date();
            //console.log();
            var special = false;
            if (date.indexOf("monfri") >= 0)
            {
                if (now.getDay() >= 1 && now.getDay() <= 5)
                {
                    special = true;
                }
            }
            if (date.indexOf("satsun") >= 0)
            {
                if (now.getDay() >= 6 && now.getDay() <= 7)
                {
                    special = true;
                }
            }
            if (now.getDay() == date || date.indexOf("allweek") >= 0)
            {
                special = true;
            }

            if (special)
            {
                if (now >= start && now <= stop)
                {
                    open = true;
                } else
                {
                    if (!open)
                    {
                        open = false;
                    }
                }
            }
        }

        // $scope.update.time.opennow = open;
        // $scope.update.$save();
        //  console.log(time);
        opennow.update({opennow: open});
        return open;
    };
}

function CreateList($scope, $location, $timeout, RestaurantService, fbgeo) {
    $scope.project = {};
    initialize($scope);

    var geoRef = new Firebase(fbgeo),
            geo = new geoFire(geoRef);

    var scope_ = $scope;
    $scope.ranges = [{}];
    $scope.removeRange = function(index) {
        scope_.ranges.splice(index, 1);
    };
    $scope.addRange = function() {
        scope_.ranges.push({});
    };

    $scope.save = function() {
        if (confirm('  ยืนยันการเพิ่มข้อมูล !!!  ')) {


            var datetime = [];
            for (var index in $scope.ranges) {
                console.log($scope.ranges[index]);
                var obj = $scope.ranges[index];
                datetime.push({date: obj.date, open: obj.open, close: obj.close});
            }
            $scope.project.data.timestamp = Date.now();
            $scope.project.data.update = Date.now();


            /*RestaurantService.$add({time: datetime, data: $scope.project.data}, function(error, res) {
             console.log(error, res);
             $timeout(function() {
             var idname = {id: $scope.project.data.timestamp, name: $scope.project.data.name};
             geo.insertByLocWithId([$scope.project.data.latitude, $scope.project.data.longitude], idname.id, idname, function(success, error) {
             });
             $location.path('/');
             });
             });*/

            RestaurantService.$add({time: datetime, data: $scope.project.data}).then(function(ref) {
                var name = ref.name();                // Key name of the added data.
                console.log(name);
                $timeout(function() {
                    var idname = {id: name, name: $scope.project.data.name};
                    geo.insertByLocWithId([$scope.project.data.latitude, $scope.project.data.longitude], idname.id, idname, function(success, error) {
                    });
                    $location.path('/');
                });
            });
        }

    };

    $scope.dragEnd = function(event) {
        $scope.project.latitude = this.position.k;
        $scope.project.longitude = this.position.A;
        document.getElementById('lat').value = this.position.k;
        document.getElementById('lng').value = this.position.A;
        console.log($scope);
    }
}

function MapController($scope, $menuService)
{
    console.log($menuService);
    $menuService.listactive = "";
    $menuService.mapactive = "active";
    console.log($menuService);
    initializeMap();



}
function EditList($scope, $location, $routeParams, $firebase, fbURL, fbgeo) {
    var projectUrl = fbURL + $routeParams.projectId;
    console.log(projectUrl);
    var dataRef = new Firebase(projectUrl);
    var latitude;
    var longitude;
    var timestamp;
    var date;
    var name;
    $scope.project = $firebase(dataRef);
    console.log($scope.project);
    var geoRef = new Firebase(fbgeo),
            geo = new geoFire(geoRef);
    dataRef.on('value', function(snapshot) {
        if (snapshot.val() != null) {

            latitude = snapshot.val().data.latitude;
            longitude = snapshot.val().data.longitude;
            timestamp = snapshot.val().data.timestamp;
            name = snapshot.val().data.name;
            addMap(latitude, longitude, 'mapCanvas', true, $scope);
            date = snapshot.val().time;
            $scope.ranges = date;
            if (date === undefined)
            {
                $scope.ranges = [{}];
            }

        }
    });
    var scope_ = $scope;
    $scope.removeRange = function(index) {
        scope_.ranges.splice(index, 1);
    };
    $scope.addRange = function() {
        scope_.ranges.push({});
    };
    $scope.destroy = function() {
        if (confirm('  ยืนยันการลบข้อมูล !!!  ')) {

            geo.removeById($routeParams.projectId);
            $scope.project.$remove();
            $location.path('/');
        }
    };
    var idname1 = name;
    $scope.save = function(idname) {
        var datetime = [];
        idname = {id: $routeParams.projectId, name: idname1};
        for (var index in $scope.ranges) {
            console.log($scope.ranges[index]);
            var obj = $scope.ranges[index]
            datetime.push({date: obj.date, open: obj.open, close: obj.close});
            console.log(datetime);
        }
        if (confirm('  ยืนยันการแก้ไขข้อมูล !!!  ')) {

            var idname = {id: $routeParams.projectId, name: $scope.project.data.name};
            geo.insertByLocWithId([$scope.project.data.latitude, $scope.project.data.longitude], idname.id, idname, function(success, error) {
            });
            $scope.project.time = datetime;
            $scope.project.data.update = Date.now();
            $scope.project.$save();
            $location.path('/');
        }

    }
}
